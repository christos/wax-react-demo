import React, { Component } from 'react';
import Wax from 'wax/packages/wax-react';

class App extends Component {
  constructor(props) {
    super(props)
    this.update = this.update.bind(this)
    this.state = { trackChanges: false }
  }

  update () {
    this.setState({
      trackChanges: !this.state.trackChanges,
    })
  }

  render() {
    return (
      <Wax
        layout='editoria'
        trackChanges = {this.state.trackChanges}
        update={this.update}
      />
    )
  }
}

export default App
